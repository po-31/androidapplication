package gps.android.handler;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import gps.android.activity.MenuActivity;
import gps.android.exception.AutorizationException;
import gps.android.other.ShowError;
import gps.beans.Coord;
import gps.packet.PacketMarker;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HandlerCoord {

    private HandlerCoord() {
    }

    private static HandlerCoord handlerCoord;

    private static Context context;
    private static MenuActivity menuActivity;
    private static ObjectInputStream is;
    private static ObjectOutputStream os;
    private static ArrayList<Coord> localStorage = new ArrayList<Coord>();
    private static Socket socket;
    private static boolean isTracked = false;
    private static boolean isOnline = false;
    private static boolean isGpsAvaliable = true;
    private static LocationManager locationManager;
    private static LocationListener locationListener;
    private static String deviceId;

    public static PacketMarker connect(String host, int port, String imei) throws IOException, OptionalDataException, ClassNotFoundException {
        
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, new LocationListener() {
            public void onLocationChanged(Location location) {}
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        });
        
        closeAll();
        InetAddress addr;
        addr = InetAddress.getByName(host);
        socket = new Socket();
        socket.connect(new InetSocketAddress(addr, port), 5000);
        os = new ObjectOutputStream(socket.getOutputStream());
        is = new ObjectInputStream(socket.getInputStream());
        PacketMarker packetMarker;
        os.writeObject(imei);
        packetMarker = (PacketMarker) is.readObject();
        if (packetMarker == PacketMarker.ERROR) {
            throw new IOException("Ошибка на сервере");
        }
        deviceId = imei;
        return packetMarker;
    }

    public static PacketMarker login(String password) throws ClassNotFoundException, IOException, AutorizationException, Exception {
        PacketMarker packetMarker;
        os.writeObject(password);
        packetMarker = (PacketMarker) is.readObject();
        if (packetMarker == PacketMarker.ERROR) {
            throw new IOException("Ошибка на сервере");
        }
        if (packetMarker == PacketMarker.SHORT_PASSWORD) {
            throw new AutorizationException("Слишком короткий пароль");
        }
        isOnline = true;

        try {
            loadCoordsFromStorage();
            while (!localStorage.isEmpty() && isOnline) {
                os.writeObject(localStorage.get(0));
                if (isOnline && ((PacketMarker) is.readObject()) != PacketMarker.OK) {
                    throw new Exception("Обрыв связи");
                }
                localStorage.remove(0);
            }
        } catch (Exception e) {
        }

        return packetMarker;
    }

    public static void track() {
        if (isTracked) {
            locationManager.removeUpdates(locationListener);
        } else {
            isGpsAvaliable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 0, locationListener);
        }
        isTracked = !isTracked;
    }

    public static void openLocalStorage(int mode) throws IOException {
        closeAll();
        if (mode == Context.MODE_PRIVATE) {
            os = new ObjectOutputStream(context.openFileOutput("coordstorage", mode));
        } else if (mode == Context.MODE_APPEND) {
            os = new AppendingObjectOutputStream(context.openFileOutput("coordstorage", mode));
        }
        while (!localStorage.isEmpty()) {
            os.writeObject(localStorage.get(0));
            localStorage.remove(0);
        }
        isOnline = false;
        if (menuActivity != null) {
            menuActivity.offOnline();
        }
    }

    public static void closeAll() {
        if (isTracked) {
            isTracked = false;
            locationManager.removeUpdates(locationListener);
        }
        try {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(HandlerCoord.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            is = null;
            os = null;
            socket = null;
        }
    }

    public static HandlerCoord getHandlerCoord() {
        if (handlerCoord == null) {
            handlerCoord = new HandlerCoord();
        }
        return handlerCoord;
    }

    public static void setContext(Context context) {
        HandlerCoord.context = context;
    }

    public static void setLocationListner() {
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                try {
                    if ((location.getProvider().equals(LocationManager.GPS_PROVIDER) && isGpsAvaliable)
                            || (location.getProvider().equals(LocationManager.NETWORK_PROVIDER) && !isGpsAvaliable)) {
                        Coord coord = new Coord(location.getLatitude(), location.getLongitude(), new Date(location.getTime()), deviceId);
                        coord.setHash(coord.hashCode());
                        os.writeObject(coord);
                        if (isOnline && ((PacketMarker) is.readObject()) != PacketMarker.OK) {
                            throw new Exception("Обрыв связи");
                        }
                    }
//                    if((location.getProvider().equals(LocationManager.NETWORK_PROVIDER) && 
//                            (!isGpsAvaliable || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) || 
//                            location.getProvider().equals(LocationManager.GPS_PROVIDER)){
//                        Coord coord = new Coord(location.getLatitude(), location.getLongitude(), new Date(location.getTime()), deviceId);
//                        coord.setHash(coord.hashCode());
//                        os.writeObject(coord);
//                        if (isOnline && ((PacketMarker) is.readObject()) != PacketMarker.OK) {
//                            throw new Exception("Обрыв связи");
//                        }
//                    }
                } catch (Exception ex) {
                    if (isOnline) {
                        ShowError.errorWithoutExit("Обрыв связи, переходим в режим оффлайн: ", menuActivity, ex);
                        try {
                            openLocalStorage(Context.MODE_PRIVATE);
                        } catch (IOException ex1) {
                            Logger.getLogger(HandlerCoord.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    }
                    Logger.getLogger(HandlerCoord.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                if (provider.equals(LocationManager.GPS_PROVIDER)) {
                    isGpsAvaliable = LocationProvider.AVAILABLE == status;
                }
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
    }

    public static void setMenuActivity(MenuActivity menuActivity) {
        HandlerCoord.menuActivity = menuActivity;
    }

    public static void loadCoordsFromStorage() throws ClassNotFoundException, IOException {
        File coordStorage = new File(context.getFilesDir() + "/coordstorage");
        if (isOnline && coordStorage.exists()) {
            ObjectInputStream localStorageIS = new ObjectInputStream(context.openFileInput("coordstorage"));
            try {
                while (true) {
                    localStorage.add((Coord) localStorageIS.readObject());
                }
            } catch (EOFException e) {
                try {
                    localStorageIS.close();
                } catch (IOException ex) {
                    Logger.getLogger(HandlerCoord.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void loadCoordsInStorage() throws IOException {
        if (isOnline) {
            ObjectOutputStream localStorageOS = new ObjectOutputStream(context.openFileOutput("coordstorage", Context.MODE_PRIVATE));

            while (!localStorage.isEmpty()) {
                localStorageOS.writeObject(localStorage.get(0));
                localStorage.remove(0);
            }
            localStorageOS.close();
        }
    }

    public static class AppendingObjectOutputStream extends ObjectOutputStream {

        public AppendingObjectOutputStream(OutputStream out) throws IOException {
            super(out);
        }

        @Override
        protected void writeStreamHeader() throws IOException {
            // do not write a header, but reset:
            // this line added after another question
            // showed a problem with the original
            reset();
        }

    }

}
