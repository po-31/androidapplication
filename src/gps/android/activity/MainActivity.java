package gps.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import gps.android.R;
import gps.android.exception.AutorizationException;
import gps.android.handler.HandlerCoord;
import gps.android.other.ShowError;
import gps.packet.PacketMarker;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends Activity {

    private String host;
    private int port;
    private String deviceId;
    private boolean isRegisrationRequired;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //создаём активити
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
                
        HandlerCoord.setContext(this);
        
        //если еще нету файла с кофигурацией соединения, создаём его
        File configConnectionFile = new File(getFilesDir() + "/configconnetcion");
        if (!configConnectionFile.exists()) {
            try {
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(openFileOutput("configconnetcion", MODE_PRIVATE)));
                bw.write("127.0.0.1 12162");
                bw.close();
            } catch (FileNotFoundException ex) {
                ShowError.error("Ошибка создания файла конфигурации: ", this, ex);
            } catch (IOException ex) {
                ShowError.error("Ошибка записи файла конфигурации: ", this, ex);
            }
        }

        //получаем DeviceID телефона
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        deviceId = telephonyManager.getDeviceId();

        //считаем конфигурацию соединения
        String[] config = null;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput("configconnetcion")));
            config = br.readLine().split(" ");
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка открытия файла конфигурации: ", this, ex);
        } catch (IOException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка записи файла конфигурации: ", this, ex);
        }
        host = config[0];
        port = Integer.parseInt(config[1]);

        connect();
    }

    private void connect() {
        //соединения с сервером
        try {
            PacketMarker registrationRequired = HandlerCoord.connect(host, port, deviceId);
            isRegisrationRequired = registrationRequired == PacketMarker.REGISTER_REQUEST;
            enableComponentsForOnline();
        } catch (IOException ex) {
            disableComponentsForOnline();
            ShowError.errorWithoutExit("Ошибка соединения с сервером: ", this, ex);
            Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            disableComponentsForOnline();
            ShowError.errorWithoutExit("Внутренняя ошибка программы: ", this, ex);
            Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void enableComponentsForOnline() {
        ((TextView) findViewById(R.id.logintext)).setText(isRegisrationRequired ? "Введите пароль для регистрации" : "Введите пароль для авторизации");
        findViewById(R.id.passwordedit).setEnabled(true);
        findViewById(R.id.loginButton).setEnabled(true);
    }

    private void disableComponentsForOnline() {
        ((TextView) findViewById(R.id.logintext)).setText("Не получается соединиться с сервером");
        findViewById(R.id.passwordedit).setEnabled(false);
        findViewById(R.id.loginButton).setEnabled(false);
    }

    public void loginOnline(View view) {
        Intent intent = new Intent(MainActivity.this, MenuActivity.class);
        try {
            PacketMarker autorizationResult = HandlerCoord.login(((EditText) findViewById(R.id.passwordedit)).getText().toString());
            if(autorizationResult != PacketMarker.AUTORIZATION_OK) throw new AutorizationException("Неверный пароль для IMEI телефона");
        } catch (Exception ex) {
            disableComponentsForOnline();
            ShowError.errorWithoutExit("Внутренняя ошибка программы: ", this, ex);
            Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        intent.putExtra("isOnline", true);
        intent.putExtra("deviceId", deviceId);
        intent.putExtra("password", ((EditText) findViewById(R.id.passwordedit)).getText().toString());
        intent.putExtra("host", host);
        startActivityForResult(intent, 2);
    }

    public void loginOffline(View view) {
        Intent intent = new Intent(MainActivity.this, MenuActivity.class);
        intent.putExtra("online", false);
        intent.putExtra("deviceId", deviceId);
        try {
            HandlerCoord.openLocalStorage(Context.MODE_APPEND);
        } catch (IOException ex) {
            ShowError.errorWithoutExit("Ошибка открытия локального хранилища: ", this, ex);
            Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
        startActivityForResult(intent, 2);
    }

    public void connectionConfig(View view) {
        Intent intent = new Intent(MainActivity.this, ConfigActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            host = data.getStringExtra("host");
            port = data.getIntExtra("port", 0);
        }
        connect();
    }

    @Override
    protected void onDestroy() {
        HandlerCoord.closeAll();
        super.onDestroy();
    }

}
