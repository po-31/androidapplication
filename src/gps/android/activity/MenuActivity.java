
package gps.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import gps.android.R;
import gps.android.handler.HandlerCoord;
import gps.android.other.ShowError;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuActivity extends Activity {
    
    private boolean isOnline; 
    private boolean isTrack = false;
    private String deviceId, password, host;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);  
        setContentView(R.layout.menu);
        HandlerCoord.setMenuActivity(this);
        HandlerCoord.setLocationListner();
        
        isOnline = getIntent().getBooleanExtra("isOnline", false);
        deviceId = getIntent().getStringExtra("deviceId");
        password = getIntent().getStringExtra("password");
        host = getIntent().getStringExtra("host");
        
        if(!isOnline){
            Button mapButton = (Button) findViewById(R.id.mapbutton);
            mapButton.setVisibility(View.INVISIBLE);
        }
        
        TextView isOnlineText = (TextView)findViewById(R.id.isonlinetext);
        isOnlineText.setText(isOnline ? "Вы работаете в онлайн режиме" : "Вы работаете в оффлайн режиме");
        TextView deviceIdText = (TextView)findViewById(R.id.deviceidtext);
        deviceIdText.setText("Ваш DeviceID: " + deviceId);
    }
    
        
    public void map(View view) {
        Intent intent = new Intent(MenuActivity.this, MapActivity.class);
        intent.putExtra("deviceId", deviceId);
        intent.putExtra("password", password);
        intent.putExtra("host", host);
        startActivity(intent);
    }
    
    public void track(View view) {
        ((Button)findViewById(R.id.trackbutton)).setText(isTrack ? "Начать отслеживать путь" : "Прекратить отслеживать путь");
        isTrack = !isTrack;
        HandlerCoord.track();
    }
    
    @Override
    public void onBackPressed() {
        HandlerCoord.closeAll();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        try {
            HandlerCoord.loadCoordsInStorage();
        } catch (IOException ex) {
            ShowError.errorWithoutExit("Ошибка записи в локальное хранилище: ", this, ex);
            Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.onStop();
    }  

    public void offOnline() {
        this.isOnline = false;
        this.isTrack = false;
        ((Button)findViewById(R.id.trackbutton)).setText("Начать отслеживать путь");
        if(isOnline) ((Button)findViewById(R.id.mapbutton)).setVisibility(View.VISIBLE);
        else ((Button)findViewById(R.id.mapbutton)).setVisibility(View.INVISIBLE);
    }
    
    
    
}
