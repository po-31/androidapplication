
package gps.android.activity;

import android.app.Activity;
import static android.content.Context.MODE_PRIVATE;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import gps.android.other.PartialRegexInputFilter;
import gps.android.R;
import gps.android.other.ShowError;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConfigActivity extends Activity {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.config);
        String[] config = null;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput("configconnetcion")));
            config = br.readLine().split(" ");
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка открытия файла конфигурации: ", this, ex);
        } catch (IOException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка записи файла конфигурации: ", this, ex);
        }
        
        EditText host = (EditText)findViewById(R.id.host);
        host.setText(config[0]);
        host.setFilters(new InputFilter[] {new PartialRegexInputFilter("(\\d{1,3}\\.){3}\\d{1,3}")});
        EditText port = (EditText)findViewById(R.id.port);
        port.setText(config[1]);
        port.setFilters(new InputFilter[] {new PartialRegexInputFilter("\\d{1,5}")});
    }
    
    public void saveConneсtionConfig(View view){
        EditText host = (EditText)findViewById(R.id.host);
        EditText port = (EditText)findViewById(R.id.port);
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(openFileOutput("configconnetcion", MODE_PRIVATE)));
            bw.write(host.getText() + " " + port.getText());
            bw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка открытия файла конфигурации: ", this, ex);
        } catch (IOException ex) {
            Logger.getLogger(MenuActivity.class.getName()).log(Level.SEVERE, null, ex);
            ShowError.error("Ошибка записи файла конфигурации: ", this, ex);
        }
        Intent intent = new Intent();
        intent.putExtra("host", host.getText().toString());
        intent.putExtra("port", Integer.parseInt(port.getText().toString()));
        setResult(RESULT_OK, intent);
        ConfigActivity.this.finish();
    }
    
}
