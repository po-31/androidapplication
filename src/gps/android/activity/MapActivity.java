    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gps.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import gps.android.other.MapView;
import gps.android.R;
import org.apache.http.util.EncodingUtils;

/**
 *
 * @author Evgeniy
 */
public class MapActivity extends Activity {

    private WebView mapView;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.map);

        mapView = (WebView) findViewById(R.id.map);
        mapView.setWebViewClient(new MapView());
        mapView.getSettings().setJavaScriptEnabled(true);
        
        String deviceId = getIntent().getStringExtra("deviceId");
        String password = getIntent().getStringExtra("password");
        String host = getIntent().getStringExtra("host");
        
//        String url = "http://192.168.1.69:8080/webserver";
        String url = "http://" + host + ":8080/web-server";
//        String postData = "username=" + deviceId + "&password=" + password;
//        mapView.postUrl(url,EncodingUtils.getBytes(postData, "BASE64"));
        mapView.loadUrl(url);  
    }
    
}
