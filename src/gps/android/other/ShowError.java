
package gps.android.other;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;


public class ShowError {
    public static void error(String errorMessage, final Activity activity, Exception exception){
        AlertDialog.Builder builder = new AlertDialog.Builder( activity );
        builder
            .setMessage( errorMessage + exception.getMessage() )
            .setCancelable( false )
            .setNeutralButton( "Ок", new DialogInterface.OnClickListener()
            {
                public void onClick ( DialogInterface dialog, int which )
                {
                    activity.finish();
                }
            } );
        AlertDialog error = builder.create();
        error.show();
    }
    
    public static void errorWithoutExit(String errorMessage, final Activity activity, Exception exception){
        AlertDialog.Builder builder = new AlertDialog.Builder( activity );
        builder
            .setMessage( errorMessage + exception.getMessage() )
            .setCancelable( false )
            .setNeutralButton( "Ок", new DialogInterface.OnClickListener()
            {
                public void onClick ( DialogInterface dialog, int which )
                {
                    
                }
            } );
        AlertDialog error = builder.create();
        error.show();
    }
}
