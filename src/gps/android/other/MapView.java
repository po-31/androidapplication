
package gps.android.other;

import android.webkit.WebView;
import android.webkit.WebViewClient;


public class MapView extends WebViewClient{ 
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) 
        {
            view.loadUrl(url);
            return true;
        }
        
}
