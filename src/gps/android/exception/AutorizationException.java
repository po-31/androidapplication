
package gps.android.exception;


public class AutorizationException extends Exception {

    public AutorizationException() {
    }

    public AutorizationException(String detailMessage) {
        super(detailMessage);
    }
    
    
}
