package gps.packet;

import java.io.Serializable;


public enum PacketMarker implements Serializable{
    PASSWORD_REQUEST, REGISTER_REQUEST, AUTORIZATION_OK, INVALID_PASSWORD, COORDINATE, ERROR, OK, SHORT_PASSWORD
}